package artest.com.artest.interfaces;

/**
 * Created by darkplataform on 27/12/2016.
 */

public interface OnAzimuthChangedListener {
    void onAzimuthChanged(float azimuthFrom, float azimuthTo);
}
