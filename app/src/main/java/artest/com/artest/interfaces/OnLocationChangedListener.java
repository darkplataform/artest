package artest.com.artest.interfaces;

import android.location.Location;

/**
 * Created by darkplataform on 27/12/2016.
 */

public interface OnLocationChangedListener {
    void onLocationChanged(Location currentLocation);
}